LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE    := tutorial-3
LOCAL_SRC_FILES := tutorial-3.c
LOCAL_STATIC_LIBRARIES := android_support
LOCAL_SHARED_LIBRARIES := gstreamer_android 
LOCAL_LDLIBS := -llog -landroid -lGLESv2 -lEGL
include $(BUILD_SHARED_LIBRARY)

GSTREAMER_ROOT_ANDROID := $(LOCAL_PATH)/armv7/

ifndef GSTREAMER_ROOT
ifndef GSTREAMER_ROOT_ANDROID
$(error GSTREAMER_ROOT_ANDROID is not defined!)
endif
GSTREAMER_ROOT := $(GSTREAMER_ROOT_ANDROID)
endif

GSTREAMER_NDK_BUILD_PATH  := $(GSTREAMER_ROOT)/share/gst-android/ndk-build/
GSTREAMER_INCLUDE_CA_CERTIFICATES := no
include $(GSTREAMER_NDK_BUILD_PATH)/plugins.mk
# CORE
GSTREAMER_PLUGINS := coreelements autodetect videorate
# NET
GSTREAMER_PLUGINS += tcp rtsp rtp rtpmanager udp
# PLAYBACK
GSTREAMER_PLUGINS += playback opengl androidmedia videoparsersbad imxvpu imxeglvivsink
GSTREAMER_EXTRA_DEPS := libgstimxvpu libfslvpuwrap libgstimxcommon libimxvpuapi libgstimxeglvivsink

include $(GSTREAMER_NDK_BUILD_PATH)/gstreamer-1.0.mk

$(call import-module,android/support)
