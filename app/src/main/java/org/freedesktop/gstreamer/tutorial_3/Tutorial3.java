package org.freedesktop.gstreamer.tutorials.tutorial_3;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.text.*;
import android.text.method.*;
import android.text.style.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import org.freedesktop.gstreamer.GStreamer;
import org.freedesktop.gstreamer.tutorials.tutorial_3.GStreamerSurfaceView;

public class Tutorial3 extends Activity {
    private native void nativeFinalize(int id); // Destroy pipeline and shutdown native code
    private native void nativePlay(int id);     // Set pipeline to PLAYING
    private native void nativePause(int id);    // Set pipeline to PAUSED
    private static native boolean nativeClassInit(); // Initialize native class: cache Method IDs for callbacks
    private native void nativeSurfaceInit(Object surface, int id, String address, int latency); // Initialize native code, build pipeline, surface, etc
    private native void nativeSurfaceFinalize(int id);

    private boolean[] is_playing_desired = new boolean[4];   // Whether the user asked to go to PLAYING
    
    public final int MAX_SURFACES = 4;
    
    private Handler handler = null;
    
    public static String getStringExtraFromIntent( Intent intent, String extraString, String ifNotFound )
    {
        String ret = intent.getStringExtra( extraString );
        if( ret == null )
        {
            ret = ifNotFound;
        }
        return ret;
    }
   
    public static String address0;
    public static String address1;
    public static String address2;
    public static String address3;
    
    private class GStreamerSurfaceHolderCallback implements SurfaceHolder.Callback
    {
        private int m_surface;
        private String m_address;
    
        public GStreamerSurfaceHolderCallback(int surface, String address)
        {
            super();
            m_surface = surface;
            m_address = address;        
        }
    
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height)
        {
            Log.d("GStreamer", String.format("Surface %d changed to format format=%d width=%d height=%d", m_surface, format, width, height));
            nativeSurfaceInit (holder.getSurface(), m_surface, m_address, 0);
        }

        public void surfaceCreated(SurfaceHolder holder)
        {
            Log.d("GStreamer", "Surface " + m_surface + " created: " + holder.getSurface());
        }

        public void surfaceDestroyed(SurfaceHolder holder)
        {
            Log.d("GStreamer", "Surface " + m_surface + " destroyed: " + holder.getSurface());
            nativeSurfaceFinalize (m_surface);
        }
    }
    
    private class SurfaceRunnable implements Runnable
    {
        Activity m_act;
        int m_id;
        int m_num;
        String m_address;
        public SurfaceRunnable( Activity act, int id, int num, String address )
        {
            super();
            m_act = act;
            m_id = id;
            m_num = num;
            m_address = address;
        }
        
        @Override
        public void run()
        {
            ((GStreamerSurfaceView) m_act.findViewById(m_id)).init().addCallback(new GStreamerSurfaceHolderCallback(m_num, m_address));
        }
    }
    
    // Called when the activity is first created.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setTheme( 0x01030224 );

        // Initialize GStreamer and warn if it fails
        try {
            GStreamer.init(this);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        setContentView(R.layout.main);
        Intent intent = getIntent();
        
        address0 = getStringExtraFromIntent(intent, "address0", "192.168.1.11:554/stander/livestream/0/1");
        address1 = getStringExtraFromIntent(intent, "address1", "192.168.1.12:554/stander/livestream/0/1");
        address2 = getStringExtraFromIntent(intent, "address2", "192.168.1.13:554/stander/livestream/0/1");
        address3 = getStringExtraFromIntent(intent, "address3", "192.168.1.14:554/stander/livestream/0/1");    
        
        // start in second delay to initialize GL
        new SurfaceRunnable(this, R.id.surface_video_0, 0, address0).run();
        new SurfaceRunnable(this, R.id.surface_video_1, 1, address1).run();
        new SurfaceRunnable(this, R.id.surface_video_2, 2, address2).run();
        new SurfaceRunnable(this, R.id.surface_video_3, 3, address3).run();
            
        if (savedInstanceState != null)
        {
            for( int i = 0; i < MAX_SURFACES; i++ )
            {
                is_playing_desired[i] = savedInstanceState.getBoolean("playing" + i);
                Log.i ("GStreamer", "Activity created. Saved state is playing:" + is_playing_desired[i]);
            }
            
        }
        else
        {
            for( int i = 0; i < MAX_SURFACES; i++ )
                is_playing_desired[i] = false;
            Log.i ("GStreamer", "Activity created. There is no saved state, playing: false");
        }
    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
        Log.d ("GStreamer", "Saving state, playing:" + is_playing_desired);
        for( int i = 0; i < MAX_SURFACES; i++ )
        {
            outState.putBoolean("playing" + i, is_playing_desired[i]);
        }
    }
    
    // Called from native code. This sets the content of the TextView from the UI thread.
    private void setMessage(final String message) {
        final TextView tv = (TextView) this.findViewById(R.id.textview_message);
        runOnUiThread (new Runnable() {
            public void run() {
                tv.setText(message);
            }
        });
    }

    // Called from native code. Native code calls this once it has created its pipeline and
    // the main loop is running, so it is ready to accept commands.
    private void onGStreamerInitialized () {
        Log.i ("GStreamer", "Gst initialized. Restoring state, playing:" + is_playing_desired);
        // Restore previous playing state
        for( int i = 0; i < MAX_SURFACES; i++ )
        {
            if (is_playing_desired[i]) {
                nativePlay(i);
            } else {
                nativePause(i);
            }
        }
        
    }
    
    @Override
    public void onBackPressed()
    {
	finish();
    }
        

    static {
        System.loadLibrary("gstreamer_android");
        System.loadLibrary("tutorial-3");
        nativeClassInit();
    }


}
