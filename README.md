# Как собирать

1. Нужен Android SDK и Android NDK.
2. Взять gstreamer-android-imx.tar.xz из foond/Обмен информацией/Omarov
3. Распаковать в app/src/main/jni
4. Подключить устройство и запустить Gradle: ./gradle installDebug

Наверное можно из Android Studio, но я им не пользуюсь и не проверял.
